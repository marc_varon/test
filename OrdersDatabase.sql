-- author       Marc Varon
-- date         2018/04/21
-------------------------------

DROP TABLE IF EXISTS order_products;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS guests;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS accounts;
DROP TYPE  IF EXISTS client_type;

CREATE TYPE client_type AS ENUM ('account', 'user', 'guest');
CREATE TABLE orders (
    id              SERIAL PRIMARY KEY,
    client_id       INTEGER NOT NULL,
    client_type     client_type NOT NULL,
    net_amount      NUMERIC (10, 2) NOT NULL,
    tax_amount      NUMERIC (10, 2) NOT NULL,
    created_at      TIMESTAMP DEFAULT now()
);
CREATE UNIQUE INDEX orders_unique_idx ON orders (client_id, client_type);
CREATE INDEX orders_creation_time_date_idx ON orders ((created_at::DATE));
INSERT INTO orders (client_id, client_type, net_amount, tax_amount, created_at) VALUES
    (1, 'guest', 100, 110, DEFAULT),
    (1, 'account', 100, 110, DEFAULT),
    (1, 'user', 100, 110, DEFAULT);

CREATE TABLE products (
    id              SERIAL PRIMARY KEY,
    category_id     INTEGER NOT NULL,
    name            VARCHAR(255) NOT NULL,
    net_price       NUMERIC (10, 2) NOT NULL,
    tax_percent     NUMERIC (10, 2) NOT NULL,
    created_at      TIMESTAMP DEFAULT now()
);
CREATE INDEX products_category_id_idx ON products (category_id);
INSERT INTO products (category_id, name, net_price, tax_percent, created_at) VALUES
    (1, 'product1', 50, 10, DEFAULT),
    (1, 'product2', 50, 10, DEFAULT),
    (1, 'product4', 50, 10, DEFAULT);

CREATE TABLE order_products (
        order_id    INTEGER references orders(id),
        product_id  INTEGER references products(id),
        quantity    INTEGER,
        PRIMARY KEY(order_id, product_id)
);
INSERT INTO order_products (order_id, product_id, quantity) VALUES
    (1, 1, 1),
    (1, 2, 1),
    (2, 1, 1),
    (2, 2, 1),
    (3, 1, 1),
    (3, 2, 1);

CREATE TABLE guests (
    id              SERIAL PRIMARY KEY,
    first_name      VARCHAR(255) NOT NULL,
    last_name       VARCHAR(255) NOT NULL,
    email           VARCHAR(255) NOT NULL,
    phone           VARCHAR(255) NOT NULL,
    national_id     VARCHAR(255) NOT NULL,
    created_at      TIMESTAMP DEFAULT now()
);
CREATE UNIQUE INDEX guests_national_id_unique_idx ON guests (national_id);
CREATE INDEX guests_email_idx ON guests (email);
INSERT INTO guests (first_name, last_name, email, phone, national_id, created_at) VALUES
    ('name1', 'surname1', 'email1@email.com', '930010011', 'X22344572E', DEFAULT),
    ('name2', 'surname2', 'email2@email.com', '930010012', 'X22344572F', DEFAULT),
    ('name3', 'surname3', 'email3@email.com', '930010013', 'X22344572G', DEFAULT);

CREATE TABLE users (
    id              SERIAL primary key,
    username        VARCHAR(40) NOT NULL,
    password        VARCHAR(255) NOT NULL,
    first_name      VARCHAR(255) NOT NULL,
    last_name       VARCHAR(255) NOT NULL,
    email           VARCHAR(255) NOT NULL,
    phone           VARCHAR(255) NOT NULL,
    national_id     VARCHAR(255) NOT NULL,
    created_at      TIMESTAMP DEFAULT now()
);
CREATE UNIQUE INDEX users_national_id_unique_idx ON users (national_id);
CREATE UNIQUE INDEX users_username_unique_idx ON users (username);
CREATE INDEX users_email_idx ON users (email);
INSERT INTO users (username, password, first_name, last_name, email, phone, national_id, created_at) VALUES
    ('user1', 'f78fdb18bf39b23d42313edfaf7e0a44', 'name1', 'surname1', 'email1@email.com', '930010011', 'X22344572E', DEFAULT),
    ('user2', 'f78fdb18bf39b23d42313edfaf7e0a45', 'name2', 'surname2', 'email2@email.com', '930010012', 'X22344572F', DEFAULT),
    ('user3', 'f78fdb18bf39b23d42313edfaf7e0a46', 'name3', 'surname3', 'email3@email.com', '930010013', 'X22344572G', DEFAULT);

CREATE TABLE accounts (
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(255) NOT NULL,
    account_number  VARCHAR(255) NOT NULL,
    created_at      TIMESTAMP DEFAULT now()
);
CREATE UNIQUE INDEX acounts_account_number_unique_idx ON accounts (account_number);
INSERT INTO accounts (name, account_number, created_at) VALUES
    ('account1', 'ES90208110080000001039531801', DEFAULT),
    ('account2', 'ES90208110080000001039531802', DEFAULT),
    ('account3', 'ES90208110080000001039531803', DEFAULT);

CREATE OR REPLACE FUNCTION generate_invoice_header(_order_id integer) 
    RETURNS void
   AS
$$
DECLARE
  query varchar;
  col_client_type client_type;
  join_fields varchar;
  join_query varchar;
BEGIN

  SELECT o.client_type into col_client_type FROM orders o WHERE id = _order_id;

  CASE col_client_type
    WHEN 'guest' THEN
      join_fields = ' , g.first_name, g.last_name, g.email, g.phone, g.national_id ';
      join_query = ' LEFT JOIN guests g ON (g.id = o.client_id) ';
    WHEN 'user' THEN
      join_fields = ' , u.username, u.first_name, u.last_name, u.email, u.phone, u.national_id ';
      join_query = ' LEFT JOIN users u ON (u.id = o.client_id) ';
    WHEN 'account' THEN
      join_fields = ' , a.name, a.account_number ';
      join_query = ' LEFT JOIN accounts a ON (a.id = o.client_id) ';
  END CASE;
  
  query := 'SELECT o.id as order_id, o.client_id, o.client_type, o.net_amount, o.tax_amount, o.created_at ' ||join_fields || '
        FROM orders o ' || join_query ;

  query := query || ' WHERE o.id = ' || _order_id || ';';

  EXECUTE 'CREATE TEMP TABLE tmp_order_invoice_' || _order_id || ' ON COMMIT DROP AS ' || query;

EXECUTE query;
    RETURN;
END;
$$
  LANGUAGE 'plpgsql' ;

SELECT generate_invoice_header(3);
SELECT * FROM tmp_order_invoice_3;


