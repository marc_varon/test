<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Repositories\CityRepository;
use Illuminate\Http\Request;

/**
 * Class City
 * @package App\Http\Controllers\Tours
 */
class City extends Controller
{

    /**
     * @var CityRepository
     */
    private $repository;

    /**
     * City constructor.
     * @param CityRepository $repository
     */
    public function __construct(CityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->repository->findByQuery($request->attributes->get('queryParams'));
    }
}
