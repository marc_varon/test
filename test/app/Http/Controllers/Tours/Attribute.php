<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Repositories\AttributeRepository;
use Illuminate\Http\Request;

/**
 * Class Attribute
 * @package App\Http\Controllers\Tours
 */
class Attribute extends Controller
{

    /**
     * @var AttributeRepository
     */
    private $repository;

    /**
     * Attribute constructor.
     * @param AttributeRepository $repository
     */
    public function __construct(AttributeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->repository->findByQuery($request->attributes->get('queryParams'));
    }
}
