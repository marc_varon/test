<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Repositories\HotelRepository;
use Illuminate\Http\Request;

/**
 * Class Hotels
 * @package App\Http\Controllers\Tours
 */
class Hotel extends Controller
{

    /**
     * @var HotelRepository
     */
    private $repository;

    /**
     * Hotel constructor.
     * @param HotelRepository $repository
     */
    public function __construct(HotelRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->repository->findByQuery($request->attributes->get('queryParams'));
    }
}
