<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Repositories\HotelRepository;
use App\Services\ToursService;
use Illuminate\Http\Request;

/**
 * Class Tour
 * @package App\Http\Controllers\Tours
 */
class Tour extends Controller
{
    /**
     * @param Request $request
     * @param ToursService $toursService
     * @return mixed
     */
    public function index(Request $request, ToursService $toursService)
    {
        return $toursService->getToursByQuery($request->attributes->get('queryParams'));
    }
}
