<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Repositories\CountryRepository;
use Illuminate\Http\Request;

/**
 * Class Country
 * @package App\Http\Controllers\Tours
 */
class Country extends Controller
{

    /**
     * @var CountryRepository
     */
    private $repository;

    /**
     * Country constructor.
     * @param CountryRepository $repository
     */
    public function __construct(CountryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->repository->findByQuery($request->attributes->get('queryParams'));
    }
}
