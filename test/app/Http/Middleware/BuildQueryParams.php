<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Services\QueryParams;

/**
 * Class BuildQueryParams
 * @package App\Http\Middleware
 */
class BuildQueryParams
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $queryParams = $this->getQueryParams($request->query());
        $request->attributes->add(['queryParams' => $queryParams]);

        return $next($request);
    }

    /*

     */
    /**
     * Sample queries:
     * q Query: It allows to filter the result of a collection. Brought to SQL would be equivalent to adding a WHERE to the query. The format is:
     * q=field1:value1,field2:value2 => WHERE field1 = 'value1' AND field2 = 'value2'
     * q=field:L(%value) => WHERE field LIKE '%value'
     * q=field:B(value1,value2) => WHERE field BETWEEN value1 AND value2
     * q=field:8 => WHERE search = '8'
     * q=field:null => WHERE search IS NULL
     * q=field:!null => WHERE search IS NOT NULL
     * q=field:value1;value2;value3 or q=field:(value1,value2,value3) => WHERE field IN ('value1', 'value2', 'value3')
     * q=field:!(value1,value2,value3) => WHERE field NOT IN ('value1', 'value2', 'value3')
     * q=field:LT(value1) => WHERE field < value1
     * q=field:LTE(value1) => WHERE field <= value1
     * q=field:GT(value1) => WHERE field > value1
     * q=field:GTE(value1) => WHERE field >= value1
     * l Limit: It allows to limit the number of records to list. If not indicated, a limit of 1000 is assumed. It would be equivalent to LIMIT in SQL.
     * l=50 LIMIT 50
     * p Page: It allows to indicate the page (offset) of the query.
     * p=3 In a query limited to 10 resources per page, it will show those ranging from 21 to 30
     * o Order: It allows to indicate the field that orders the results.
     * o=code DESC,id Sort by the code field in descending order, in case of coinciding by the id field
     *
     * Sample request
     * GET /tours?q=country.id:2,city.id:4,attributes.id:4;5;6&o=city.ratind DESC&l=3 HTTP/1.1
     *
     * @param array $query
     * @return QueryParams
     */
    protected function getQueryParams(array $query)
    {
        $queryParams = new QueryParams();

        if (isset($query['l'])) {
            $queryParams->limit = filter_var($query['l'], FILTER_SANITIZE_NUMBER_INT);
        }
        if (isset($query['o'])) {
            $queryParams->order = explode(',', filter_var($query['o'], FILTER_SANITIZE_STRING));
        }
        if (isset($query['p'])) {
            $queryParams->page = filter_var($query['p'], FILTER_SANITIZE_NUMBER_INT);
        }
        if (isset($query['q'])) {
            $matches = [];
            $regex = '/[^:,]+:[A-Z]?(?:\([^\)]+\))|[^,]+/';
            preg_match_all($regex, filter_var($query['q'], FILTER_SANITIZE_STRING), $matches);
            $queryParams->criteria = [];
            $conditions = $matches[0];
            foreach ($conditions as $cond) {
                $fields = explode(':', $cond);
                //Check if it has a list of elements for IN operator
                if (strpos($fields[1], ';') && strpos($fields[1], '(') === FALSE && strpos($fields[1], ')') === FALSE) {
                    $fields[1] = explode(';', $fields[1]);
                }

                if ($fields[1] === 'null') {
                    $fields[1] = null;
                }

                $queryParams->criteria[$fields[0]] = $fields[1];
            }
        }

        return $queryParams;
    }
}
