<?php

namespace App\Providers;

use App\Entities\City;
use App\Entities\Country;
use App\Entities\Hotel;
use App\Entities\Attribute;
use App\Repositories\AttributeRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\DoctrineAttributeRepository;
use App\Repositories\DoctrineCityRepository;
use App\Repositories\DoctrineCountryRepository;
use App\Repositories\DoctrineHotelRepository;
use App\Repositories\HotelRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HotelRepository::class, function($app) {
           return new DoctrineHotelRepository(
               $app['em'],
               $app['em']->getClassMetaData(Hotel::class)
           );
        });
        $this->app->bind(CountryRepository::class, function($app) {
            return new DoctrineCountryRepository(
                $app['em'],
                $app['em']->getClassMetaData(Country::class)
            );
        });
        $this->app->bind(CityRepository::class, function($app) {
            return new DoctrineCityRepository(
                $app['em'],
                $app['em']->getClassMetaData(City::class)
            );
        });
        $this->app->bind(AttributeRepository::class, function($app) {
            return new DoctrineAttributeRepository(
                $app['em'],
                $app['em']->getClassMetaData(Attribute::class)
            );
        });
    }
}
