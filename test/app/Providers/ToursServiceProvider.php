<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ToursService;

class ToursServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ToursService', function ($app) {
            return new ToursService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ToursService::class];
    }
}
