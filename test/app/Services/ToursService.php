<?php

namespace App\Services;

use App\Services\QueryParams;
use App\Entities\Hotel;
use App\Entities\Tour;
use App\Repositories\HotelRepository;

/**
 * Class ToursService
 * @package App\Services
 */
class ToursService
{

    /**
     * @param \App\Services\QueryParams $queryParams
     * @return mixed
     */
    public function getToursByQuery(QueryParams $queryParams)
    {

        $requiredRelations = ['city', 'country', 'attributes'];

        foreach ($requiredRelations as $relation) {
            if (
                empty($queryParams->criteria) ||
                (
                    !$this->arraySearchKeyByStrpos($queryParams->criteria, $relation) !== FALSE &&
                    (empty($queryParams->order) || !$this->arraySearchKeyByStrpos($queryParams->order, $relation) !== FALSE)
                )
            ) {
                $queryParams->criteria[$relation . '.id'] = '!null';
            }
        }

        /* @var $hotelRepository HotelRepository */
        $hotelRepository = \EntityManager::getRepository(Hotel::class);
        $hotels = $hotelRepository->findByQuery($queryParams);

        return array_reduce($hotels, function ($tours, $item) {

            $tour = new Tour();
            $tour->country = $item['country'];
            $tour->city = $item['city'];

            unset($item['country']);
            unset($item['city']);

            $tour->hotel = $item;
            $tours[] = $tour;

            return $tours;
        });
    }


    /**
     * @param array $array
     * @param string $string
     * @return bool
     */
    private function arraySearchKeyByStrpos(array $array, string $string)
    {

        foreach ($array as $key => $value) {
            if ($index = mb_strpos($key, $string) !== FALSE) {
                return $index;
            }
        }
        return false;
    }
}
