<?php
namespace App\Services;

/**
 * Class QueryParams
 * @package App\Services\QueryParams
 */
class QueryParams
{

    /**
     * @var array
     */
    public $criteria;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $page;

    /**
     * @var array
     */
    public $order;

}