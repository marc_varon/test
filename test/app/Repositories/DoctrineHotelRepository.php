<?php
namespace App\Repositories;

use App\Services\QueryParams;

class DoctrineHotelRepository extends DoctrineAbstractRepository implements HotelRepository
{
    public function findByQuery(QueryParams $queryParams)
    {
        return $this->query($queryParams);
    }
}