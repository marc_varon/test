<?php
namespace App\Repositories;

use Doctrine\ORM\EntityRepository;
use App\Services\QueryParams;
use Doctrine\ORM\Mapping;


/**
 * Class DoctrineAbstractRepository
 * @package App\Repositories
 */
class DoctrineAbstractRepository extends EntityRepository
{

    /**
     * @var string
     */
    private $tableName;
    /**
     * @var array
     */
    private $joinedEntities = [];

    /**
     * DoctrineAbstractRepository constructor.
     * @param \Doctrine\ORM\EntityManager|\Doctrine\ORM\EntityManagerInterface $em
     * @param Mapping\ClassMetadata $class
     */
    public function __construct($em, Mapping\ClassMetadata $class)
    {
        $this->tableName = $class->getTableName();
        parent::__construct($em, $class);
    }

    /**
     * @param QueryParams $queryParams
     * @return array
     */
    protected function query(QueryParams $queryParams)
    {
        $result = [];
        try {

            $builder = $this->getQueryBuilder($queryParams);
            if (!empty($builder)) {
                $result = $builder->getQuery()->getArrayResult();

                //LIMIT & PAGE
                if (!empty($queryParams->limit)) {
                    $this->addPagination($result, $queryParams->limit, $queryParams->page);
                }
            }

        } catch (\Exception $e) {

            if (env('APP_DEBUG', false)) {
                Log::error($e);
            }

        } finally {
            return $result;
        }
    }

    /**
     * @param QueryParams $queryParams
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getQueryBuilder(QueryParams $queryParams)
    {
        $builder = $this->createQueryBuilder($this->tableName);

        //CRITERIA
        if (!empty($queryParams->criteria)) {
            foreach (static::assureArray($queryParams->criteria) as $key => $value) {
                $this->addCriteria($builder, $key, $value);
            }
        }

        //ORDER
        if (!empty($queryParams->order)) {
            foreach (static::assureArray($queryParams->order) as $orderBy) {
                $this->addOrder($builder, $orderBy);
            }
        }


        return $builder;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $builder
     * @param $orderBy
     */
    protected function addOrder(\Doctrine\ORM\QueryBuilder $builder, $orderBy)
    {
        if (mb_strpos($orderBy, '.') !== FALSE) {
            $this->addJoin($builder, $orderBy);
        } else {
            $orderBy = $this->tableName . '.' . $orderBy;
        }
        $builder->add('orderBy', $orderBy);
    }

    /**
     * @param array $array
     * @param $limit
     * @param $offset
     */
    protected function addPagination(&$array, $limit, $offset)
    {
        /**
         * TODO Find a better solution... or refactor all class to avoid this doctrine bug
         * Hi Boris, Doctrine have a very bug here:
         * @link https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html#first-and-max-result-items-dql-query-only
         *
         * This can easily cause a performance error and can reach memory limit of php with a big resultset array
         */

        if (!is_numeric($limit)) {
            throw new \Exception("Invalid param limit " . $limit);
        }
        $array = array_slice($array, !empty($offset) ? $offset : 0, $limit);
    }

    /**
     * Exp reg '/^([A-Z]*){0,1}\(([^\)]+)\)$/' to get params type:
     *  q=field1:value1,field2:value2 => WHERE field1 = 'value1' AND field2 = 'value2'
     *  q=field:L(%value) => WHERE field LIKE '%value'
     *  q=field:B(value1,value2) => WHERE field BETWEEN value1 AND value2
     *  q=field:8 => WHERE search = '8'
     *  q=field:value1;value2;value3 o @q=field:(value1,value2,value3) => WHERE field IN ('value1', 'value2', 'value3')
     *  q=field:LT(value1) => WHERE field < value1
     *  q=field:LTE(value1) => WHERE field <= value1
     *  q=field:GT(value1) => WHERE field > value1
     *  q=field:GTE(value1) => WHERE field >= value1
     *
     * @param \Doctrine\ORM\QueryBuilder $builder
     * @param string $key
     * @param mixed $value
     * @throws \Exception
     */
    protected function addCriteria(\Doctrine\ORM\QueryBuilder $builder, $key, $value)
    {
        if (mb_strpos($key, '.') !== FALSE) {
            $this->addJoin($builder, $key);
        } else {
            $key = $this->tableName . '.' . $key;
        }

        if (is_array($value)) {
            $builder->andWhere($builder->expr()->in($key, static::fixQueryValueType($value)));
        } else {
            $matches = [];
            if (preg_match('/^([A-Z!]*){0,1}\(([^\)]+)\)$/', $value, $matches)) {
                switch ($matches[1]) {
                    case 'L' :
                        $builder->andWhere($builder->expr()->like($key, static::fixQueryValueType($matches[2])));
                        break;
                    case 'B' :
                        if (strpos($matches[2], ',') !== FALSE) {
                            $minmax = explode(',', $matches[2]);
                        } elseif (strpos($matches[2], ';') !== FALSE) {
                            $minmax = explode(';', $matches[2]);
                        }
                        if (count($minmax) !== 2) {
                            throw new \Exception("Invalid query params $matches[2]");
                        }
                        $builder->andWhere($builder->expr()->between(
                            $key, static::fixQueryValueType($minmax[0]),
                            static::fixQueryValueType($minmax[1]))
                        );
                        break;
                    case 'LT' :
                        $builder->andWhere($builder->expr()->lt($key, static::fixQueryValueType($matches[2])));
                        break;
                    case 'LTE' :
                        $builder->andWhere($builder->expr()->lte($key, static::fixQueryValueType($matches[2])));
                        break;
                    case 'GT' :
                        $builder->andWhere($builder->expr()->gt($key, static::fixQueryValueType($matches[2])));
                        break;
                    case 'GTE' :
                        $builder->andWhere($builder->expr()->gte($key, static::fixQueryValueType($matches[2])));
                        break;
                    case '!':
                        if (strpos($matches[2], ';') !== FALSE) {
                            $values = explode(';', $matches[2]);
                        } else {
                            $values = array($matches[2]);
                        }
                        if (!in_array('null', $values)) {
                            $builder->andWhere($builder->expr()->notIn($key, static::fixQueryValueType($values)));
                        } else {
                            $idx = array_search('null', $values);
                            unset($values[$idx]);
                            $builder->andWhere($builder->expr()->notIn($key, static::fixQueryValueType($values)))
                                ->andWhere($builder->expr()->isNotNull($key));
                        }

                        break;
                    case '' :
                        $builder->andWhere($builder->expr()->in($key, static::fixQueryValueType(explode(',', $matches[2]))));
                        break;
                    default:
                        throw new \Exception('Invalid query modifier ' . $matches[1]);
                }
            } else {
                if ($value === null) {
                    $builder->andWhere($builder->expr()->isNull($key));
                } elseif (is_string($value) && $value == '!null') {
                    $builder->andWhere($builder->expr()->isNotNull($key));
                } else {
                    $builder->andWhere($builder->expr()->eq($key, static::fixQueryValueType($value)));
                }
            }
        }
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $builder
     * @param $key
     */
    private function addJoin(\Doctrine\ORM\QueryBuilder $builder, $key)
    {
        $rel = explode('.', $key)[0];
        if (!in_array($rel, $this->joinedEntities)) {
            $builder->innerJoin($this->tableName . '.' . $rel, $rel)
                ->addSelect($rel);
            $this->joinedEntities[] = $rel;
        }
    }

    /**
     * @param $el
     * @return array
     */
    private static function assureArray($el)
    {
        return !is_array($el) ? [$el] : $el;
    }

    /**
     * @param $el
     * @return int|string
     */
    private static function fixQueryValueType($el)
    {
        if (is_array($el)) {
            return array_map(function ($item) {
                return static::fixQueryValueType($item);
            }, $el);
        } else {
            return $el = is_numeric($el) ? $el : '\'' . $el . '\'';
        }
    }

}