<?php
namespace App\Repositories;

use App\Services\QueryParams;

class DoctrineAttributeRepository extends DoctrineAbstractRepository implements AttributeRepository
{
    public function findByQuery(QueryParams $queryParams)
    {
        return $this->query($queryParams);
    }
}