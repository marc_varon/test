<?php
namespace App\Repositories;

use App\Services\QueryParams;

class DoctrineCityRepository extends DoctrineAbstractRepository implements CityRepository
{
    public function findByQuery(QueryParams $queryParams)
    {
        return $this->query($queryParams);
    }
}