<?php
namespace App\Repositories;

use App\Services\QueryParams;

interface CountryRepository {

    public function findByQuery(QueryParams $queryParams);
}