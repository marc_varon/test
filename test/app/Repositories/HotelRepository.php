<?php
namespace App\Repositories;

use App\Services\QueryParams;

interface HotelRepository {

    public function findByQuery(QueryParams $queryParams);
}