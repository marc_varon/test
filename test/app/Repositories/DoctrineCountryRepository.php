<?php
namespace App\Repositories;

use App\Services\QueryParams;

class DoctrineCountryRepository extends DoctrineAbstractRepository implements CountryRepository
{
    public function findByQuery(QueryParams $queryParams)
    {
        return $this->query($queryParams);
    }
}