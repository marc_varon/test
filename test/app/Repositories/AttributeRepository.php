<?php
namespace App\Repositories;

use App\Services\QueryParams;

interface AttributeRepository {

    public function findByQuery(QueryParams $queryParams);
}