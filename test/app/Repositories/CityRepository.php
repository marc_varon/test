<?php
namespace App\Repositories;

use App\Services\QueryParams;

interface CityRepository {

    public function findByQuery(QueryParams $queryParams);
}