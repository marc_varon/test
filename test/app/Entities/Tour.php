<?php
namespace App\Entities;

/**
 * Class Tour
 * @package App\Entities
 */
class Tour
{
    /**
     * @var array
     */
    public $country;
    /**
     * @var array
     */
    public $city;
    /**
     * @var array
     */
    public $hotel;
}