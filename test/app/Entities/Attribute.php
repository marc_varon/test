<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="app\Repositories\DoctrineAttributeRepository")
 * @ORM\Table(name="attributes")
 */
class Attribute
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * Many Attributes have Many Hotels.
     * @ORM\ManyToMany(targetEntity="Hotel")
     */
    protected $hotels;

    /**
     * @param $name
     * @param $stars
     */
    public function __construct($name)
    {
        $this->name  = $name;

        $this->hotels = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getHotels()
    {
        return $this->hotels;
    }
}