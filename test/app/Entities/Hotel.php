<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="hotels")
 * @ORM\Entity(repositoryClass="app\Repositories\DoctrineHotelRepository")
 */
class Hotel
{
    use BaseTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stars;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="hotels")
     * @var Country
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="hotels")
     * @var City
     */
    protected $city;

    /**
     * Many Hotel have Many Attributes.
     * @ORM\ManyToMany(targetEntity="Attribute")
     * @ORM\JoinTable(name="hotel_attributes",
     *      joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attribute_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $attributes;

    /**
     * @param $name
     * @param $stars
     */
    public function __construct($name, $stars)
    {
        $this->name  = $name;
        $this->stars  = $stars;

        $this->attributes = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getStars()
    {
        return $this->stars;
    }

    public function setStars($stars)
    {
        $this->stars = $stars;
    }

    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCity(City $city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }
}