<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="app\Repositories\DoctrineCityRepository")
 * @ORM\Table(name="cities")
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $rating;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="cities")
     * @var Country
     */
    protected $country;

    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="city", cascade={"persist"})
     * @var ArrayCollection|Hotel[]
     */
    protected $hotels;

    /**
     * @param $isoCode
     * @param $name
     */
    public function __construct($name)
    {
        $this->name  = $name;

        $this->hotels = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function addHotel(Hotel $hotel)
    {
        if(!$this->hotels->contains($hotel)) {
            $hotel->setCity($this);
            $this->hotels->add($hotel);
        }
    }

    public function getHotels()
    {
        return $this->hotels;
    }
}