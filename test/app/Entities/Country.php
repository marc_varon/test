<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="app\Repositories\DoctrineCountryRepository")
 * @ORM\Table(name="countries")
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $iso_code;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="country", cascade={"persist"})
     * @var ArrayCollection|City[]
     */
    protected $cities;

    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="country", cascade={"persist"})
     * @var ArrayCollection|Hotel[]
     */
    protected $hotels;

    /**
     * @param $isoCode
     * @param $name
     */
    public function __construct($isoCode, $name)
    {
        $this->iso_code = $isoCode;
        $this->name  = $name;

        $this->cities = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIsoCode()
    {
        return $this->iso_code;
    }

    public function setIsoCode($isoCode)
    {
        $this->iso_code = $isoCode;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function addCity(City $city)
    {
        if(!$this->cities->contains($city)) {
            $city->setCountry($this);
            $this->cities->add($city);
        }
    }

    public function getCities()
    {
        return $this->cities;
    }

    public function addHotel(Hotel $hotel)
    {
        if(!$this->hotels->contains($hotel)) {
            $hotel->setCountry($this);
            $this->hotels->add($hotel);
        }
    }

    public function getHotels()
    {
        return $this->hotels;
    }
}