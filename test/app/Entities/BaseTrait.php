<?php
namespace App\Entities;

/**
 * Class BaseTrait
 * @package App\Entities
 */
trait BaseTrait
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

}