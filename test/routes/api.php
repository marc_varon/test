<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('tours', 'Tours\Tour@index');
Route::get('hotels', 'Tours\Hotel@index');
Route::get('cities', 'Tours\City@index');
Route::get('countries', 'Tours\Country@index');
Route::get('attributes', 'Tours\Attribute@index');