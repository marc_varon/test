-- author       Marc Varon
-- date         2018/04/19
-------------------------------

DROP TABLE IF EXISTS hotel_attributes;
DROP TABLE IF EXISTS hotels;
DROP TABLE IF EXISTS attributes;
DROP TABLE IF EXISTS cities;
DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
    id              SERIAL PRIMARY KEY,
    iso_code        CHAR(2) NOT NULL,
    name            VARCHAR(255) NOT NULL
);


CREATE TABLE cities (
    id              SERIAL primary key,
    name            VARCHAR(255) NOT NULL,
    rating          INTEGER,
    country_id      INTEGER NOT NULL references countries(id)
);


CREATE TABLE hotels (
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(255) NOT NULL,
    stars           INTEGER NOT NULL,
    city_id         INTEGER NOT NULL references cities(id),
    country_id      INTEGER NOT NULL references countries(id)
);


CREATE TABLE attributes (
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(255) NOT NULL
);


CREATE TABLE hotel_attributes (
    hotel_id        INTEGER references hotels(id),
    attribute_id    INTEGER references attributes(id),
    PRIMARY KEY(hotel_id, attribute_id)
);

INSERT INTO countries (iso_code, name) VALUES
    ('ES', 'Spain'),
    ('FR', 'France'),
    ('GR', 'Greece');
    INSERT INTO cities (name, rating, country_id) VALUES
    ('Barcelona',10 , 1),
    ('Tignes',5 , 2),
    ('Mont-blanc',8 , 2),
    ('Megève',6 , 2),
    ('Isère',7 , 2),
    ('Atenas',8 , 3);
    INSERT INTO hotels (name, stars, city_id, country_id) VALUES
    ('Hotel Barcelona', 3, 1, 1),
    ('Hotel Tignes', 3, 2, 2),
    ('Hotel Mont-blanc', 3, 3, 2),
    ('Hotel Megève', 3, 4, 2),
    ('Hotel Isère', 3, 5, 2),
    ('Hotel Atenas', 3, 6, 3);
    INSERT INTO attributes (name) VALUES
    ('Close to centre of the city'),
    ('Excursions in the hotel'),
    ('Allowed with dogs'),
    ('Transfer to hotel'),
    ('Has ski rent'),
    ('Close to elevator'),
    ('Rest with kids'),
    ('Near beach'),
    ('Sand beah'),
    ('Conditioner');
    INSERT INTO hotel_attributes (hotel_id, attribute_id) VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 4),
    (2, 5),
    (2, 6),
    (3, 4),
    (3, 5),
    (3, 6),
    (4, 4),
    (4, 5),
    (4, 6),
    (5, 4),
    (5, 5),
    (5, 6),
    (6, 7),
    (6, 8),
    (6, 9),
    (6, 10);


